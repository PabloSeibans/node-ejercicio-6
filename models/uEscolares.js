const { v4: uuidv4 } = require('uuid');

class uEscolar {
  constructor(material, precio_unitario,moneda, precio1, precio2) {
    this.id_compra = uuidv4();
    this.material = material;
    this.precio_unitario = precio_unitario;
    this.moneda = moneda;
    this.precio_total = precio1 + precio2;
  }
}

module.exports = uEscolar